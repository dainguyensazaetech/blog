class CreateArticiles < ActiveRecord::Migration[7.0]
  def change
    create_table :articiles do |t|
      t.string :title
      t.string :string
      t.text :body

      t.timestamps
    end
  end
end
